package com.parser;

import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonFileContent {
	private String filePath = "";
	private String objKey = "";
	private JsonObjectValue jsonObjectValue = null;
	
	JsonFileContent(String filePath) {
		this.filePath = filePath;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(filePath));
			initJsonContent((JSONObject) obj);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
	
	public void initJsonContent(JSONObject obj) {
		int level = 1;
    	String key = (String) obj.keySet().iterator().next();
    	this.jsonObjectValue = new JsonObjectValue("", obj, level);
    }
	
	public String getOverview() {
		
		return this.jsonObjectValue.toString();
	}
	
	@Override
	public String toString() {
		return this.filePath + '\n' + this.jsonObjectValue.toString();
	}
}
