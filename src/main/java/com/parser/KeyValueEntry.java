package com.parser;

import java.util.Arrays;

public class KeyValueEntry {
	private String key;
	private String value;
	KeyValueEntry(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public String getvalue() {
		return value;
	}
	
}
