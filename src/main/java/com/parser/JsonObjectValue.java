package com.parser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonObjectValue extends Object{
	private String objectKey = "";
	private JSONObject jsonObject = new JSONObject();
	private List<KeyValueEntry> stringValues = new ArrayList<KeyValueEntry>();
	private List<JsonObjectValue> subJsonEntries = new ArrayList<JsonObjectValue>();
	private boolean isValueJson = false;
	private String thisToString = "";
	private String upperLevelObjKey;
	private int baseLevel;
	
	private void initLocalVars(String objKeyArg, JSONObject jsonObjArg, int baseLevel, String upperLevelObjKey) {
		this.objectKey = objKeyArg;
		this.jsonObject = jsonObjArg;
		this.baseLevel = baseLevel;
		this.upperLevelObjKey = upperLevelObjKey;
		this.thisToString = processJsonValue(objKeyArg, jsonObjArg, baseLevel);
	}
	
	
	JsonObjectValue(String objKeyArg, JSONObject jsonObjArg, int baseLevel) {
		initLocalVars(objKeyArg, jsonObjArg, baseLevel, "");
    }
	
	JsonObjectValue(String objKeyArg, JSONObject jsonObjArg, int baseLevel, String upperLevelObjKey) {
		initLocalVars(objKeyArg, jsonObjArg, baseLevel, upperLevelObjKey);
    }
	
	public String processJsonValue(String objKeyArg, JSONObject jsonObjArg, int baseLevel) {
		char [] indentTimes = new char[baseLevel*3];
    	Arrays.fill(indentTimes, ' ');
    	String intdentation = new String(indentTimes);
    	String messageOnConsole = this.thisToString;
    	if(upperLevelObjKey.equals("")) {
    		if(!objKeyArg.equals("")) {
    			messageOnConsole += intdentation + objKeyArg + '\n';
    			//System.out.println(intdentation + objKeyArg);
    		}
    	} else {
    		if(jsonObjArg.get(jsonObjArg.keySet().iterator().next()).getClass().getSimpleName().equals("JSONObject")) {
    			messageOnConsole += intdentation  + this.upperLevelObjKey + "." + (String) objKeyArg +  "." + jsonObjArg.keySet().iterator().next() + '\n';
    			//System.out.println(intdentation  + this.upperLevelObjKey + "." + (String) objKeyArg +  "." + jsonObjArg.keySet().iterator().next());
    		} else {
    			messageOnConsole += intdentation  + this.upperLevelObjKey + "." + (String) objKeyArg + '\n';
    			//System.out.println(intdentation  + this.upperLevelObjKey + "." + (String) objKeyArg);
    		}
    			
    	}
		baseLevel += 1;
		//System.out.println(objKeyArg + " : ");
		for(Object objKey : jsonObjArg.keySet()) {
			int level = baseLevel;
    		if(jsonObjArg.get((String) objKey).getClass().getSimpleName().equals("JSONObject")) {
    			try {
    				this.isValueJson = true;
    				JsonObjectValue newJson = new JsonObjectValue((String) objKey, (JSONObject)jsonObjArg.get((String) objKey), level, objKeyArg); 
    				this.subJsonEntries.add(newJson);
    				messageOnConsole += newJson.toString();
    			} catch (ClassCastException e) { }
        	} else {
        		this.stringValues.add(new KeyValueEntry((String) objKey, (String) jsonObjArg.get((String)objKey)));
        		messageOnConsole += intdentation + "   " + '{' +  '"' + (String) objKey +  '"' + ':' +  '"' + jsonObjArg.get((String) objKey) + '"' + '}' + '\n';
        		//System.out.println(intdentation + "   {" +  '"' + (String) objKey +  '"' + ":" +  '"' + jsonObjArg.get((String) objKey) + '"' +  "}");
        	}
    	}
		
		return messageOnConsole;
	}
	@Override
	public String toString() {
		return this.thisToString;
		
	}
}
