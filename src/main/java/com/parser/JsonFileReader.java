package com.parser;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonFileReader {
  
  private String dirOrFileLocationPath;
  private String fileName = "";
  private HashMap<String, JsonFileContent> listOfFiles = new HashMap<String, JsonFileContent>();
  private boolean isDir = false;
    JsonFileReader(String locationPath) {
        this.dirOrFileLocationPath = locationPath;
    }

    public void startParsing() {
        File dirLocation = new File(this.dirOrFileLocationPath);
        //System.out.println(this.dirOrFileLocationPath);
        if(dirLocation.list() != null) {
        	isDir = true;
        	Arrays.stream(dirLocation.list()).forEach(this::parseLocationPath);
        } else {
        	parseLocationPath("");
        }
    }
    
    public void parseLocationPath(String locationPath){
    	char slash = '/';
    	String fullPathToFile = this.dirOrFileLocationPath;
    	if(!locationPath.equals("")) {
    		fullPathToFile += slash;
        	fullPathToFile += locationPath;
    	} else {
    		locationPath = Paths.get(this.dirOrFileLocationPath).getFileName().toString();
    		this.fileName = locationPath;
    	}
    	JsonFileContent fileContent = new JsonFileContent(fullPathToFile);
    	listOfFiles.put(locationPath, fileContent);
    }

    public JsonFileContent getJsonContentOf(String filename) {
    	return this.listOfFiles.get(filename);
    }
    
    public String getWholeJsonDirContent() {
    	if(isDir) {
    		String output = "";
    		for(String key : this.listOfFiles.keySet()) {
    			output += this.listOfFiles.get(key).toString() + '\n';
    		}
    		return output;
    	} else {
    		return this.listOfFiles.get(this.fileName).toString();
    	}
    }
    
    public void writeToCsv() {
        //TODO
    }
}