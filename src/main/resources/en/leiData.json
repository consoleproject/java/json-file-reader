{
  "leiApp": {
    "tabs": {
      "paymentValidation": "1. Payment validation",
      "submitterValidation": "2. Submitter validation",
      "dataQualityValidation": "5. Data quality validation",
      "duplicateValidation": "4. Duplicate validation",
      "leiDataValidation": "3. LEI data validation",
      "legalEntity": "Legal Entity",
      "legalEntity.placeholder": "Legal Entity - mandatory data",
      "addresses": "Addresses",
      "addresses.placeholder": "Addresses - mandatory data",
      "legalAddress": "Legal Address",
      "legalAddress.placeholder": "Legal Address - mandatory data",
      "legalHeadquartersAddress": "Headquarters Address",
      "legalHeadquartersAddress.placeholder": "Headquarters Address - optional data",
      "dacRelationship": "Relationship - Direct Parent",
      "dacRelationship.placeholder": "Relationship - Direct Parent data",
      "uacRelationship": "Relationship - Ultimate Parent",
      "uacRelationship.placeholder": "Relationship - Ultimate Parent data",
      "attachments": "Enclosed Documents: {{count}}",
      "attachments.placeholder": "Enclosed Documents - optional data",
      "assistedAuth": "Assisted Authorization",
      "assistedAuth.placeholder": "For Assisted transfer only - upload auhtorization letter"
    },
    "leiData": {
      "messages": {
        "validate": {
          "lei": {
            "required": "Legal Entity Identifier is required for both save and submission."
          },
          "legalNameValue": {
            "required": "Legal Name is required for both save and submission."
          },
          "registrationAuthorityID": {
            "required": "Registration Authority ID is required."
          },
          "registrationAuthorityEntityID": {
            "required": "Registration Authority Entity ID is required."
          },
          "entityLegalForm": {
            "required": "Entity Legal Form is required."
          },
          "entityStatus": {
            "required": "Entity Status is required."
          },
          "idForVat": {
            "required": "VAT ID is required."
          },
          "entityExpirationDate": {
            "required": "Entity Expiration Date is required."
          },
          "entityExpirationReason": {
            "required": "Entity Expiration Reason is required."
          },
          "managingLOU": {
            "required": "Managing LOU is required."
          },
          "registrationCountry": {
            "required": "Legal Jurisdiction Country is required."
          },
          "registrationRegion": {
            "required": "Legal Jurisdiction Region is required."
          },
          "initialRegistrationDate": {
            "past": "Should be less or equal Last Update Date.",
            "required": "Initial Registration Date is required."
          },
          "lastUpdateDate": {
            "past": "Should be less than today.",
            "required": "Last Update Date is required."
          },
          "nextRenewalDate": {
            "required": "Next Renewal Date is required."
          },
          "successorEntityNameValue": {
            "required": "Successor Entity Name is required."
          },
          "successorLei": {
            "search": "Please search for Successor LEI."
          },
          "firstAddressLine": {
            "required": "First Address Line is required."
          },
          "city": {
            "required": "City is required."
          },
          "country": {
            "required": "Country is required."
          },
          "headquartersFirstAddressLine": {
            "required": "First Address Line is required."
          },
          "headquartersCity": {
            "required": "City is required."
          },
          "headquartersCountry": {
            "required": "Country is required."
          },
          "required.for.save": "No enough attributes passed validation in order to perform save.",
          "required.for.next": "No enough attributes passed validation in order to proceed next to relationship data.",
          "required.for.submit": "No enough attributes passed validation in order to perform submission."
        }
      },
      "home": {
        "title": "LEI Data",
        "createLabel": "Create a new LEI Data",
        "createOrEditLabel": "Create or edit a LEI Data",
        "search": "Search for LEI Data"
      },
      "validation": {
        "complete": "LEI record validation complete",
        "not.complete": "LEI record validation not complete"
      },
      "created": "A new LEI Data is created with identifier {{ param }}",
      "updated": "A LEI Data is updated with identifier {{ param }}",
      "deleted": "A LEI Data is deleted with identifier {{ param }}",
      "archived": "A LEI Data is archived with identifier {{ param }}",
      "archive": {
        "question": "Are you sure you want to archive LEI record {{ id }}?"
      },
      "renew": {
        "question": "Are you sure you want to renew LEI record {{ id }}?"
      },
      "nolei": "No LEI yet assigned",
      "process": "Process LEI record",
      "adjust": "Make adjustments of published LEI record",
      "expire": "Expiration of LEI record",
      "detail": {
        "tabs": {
          "legalEntity": "Legal Entity Information",
          "legalEntity.placeholder": "Legal Entity Information",
          "address": "Address",
          "address.placeholder": "Address",
          "relationship": "Relationship",
          "relationship.placeholder": "Relationship"
        },
        "title": "Edit LEI record - Entity data",
        "title.new": "Registration - Entity data",
        "title.renew": "Renew LEI record - Entity data"
      },
      "relationship": {
        "tabs": {
          "direct": "Relationship - Direct Parent",
          "direct.placeholder": "Relationship - Direct Parent",
          "ultimate": "Relationship - Ultimate Parent",
          "ultimate.placeholder": "Relationship - Ultimate Parent"
        },
        "title": "Edit LEI record - Relationship",
        "title.new": "Registration - Relationship"
      },
      "save": {
        "placeholder": "Save draft data as is and do not perform integrity check"
      },
      "submit": {
        "ready": "LEI record is ready for publishing.",
        "placeholder": "Save draft data, perform integrity check and submit for LEI approval",
        "title": "Edit LEI record - Submit",
        "title.new": "Registration - Submit"
      },
      "confirm": {
        "title": "Edit LEI record - Confirmation",
        "title.new": "Registration - Confirmation"
      },
      "view": {
        "title": "View LEI record"
      },
      "legalNameValue": "Legal Name",
      "legalNameValue.placeholder": "Legal Name. Mandatory field.",
      "legalNameLang": "Legal Name Lang",
      "associatedEntityNameValue": "Associated Entity Name",
      "associatedEntityNameLang": "Associated Entity Name Lang",
      "lei": "Legal Entity Identifier",
      "lei.pending": "Assignment pending...",
      "entityLegalForm": "Entity Legal Form",
      "entityLegalForm.placeholder": "Entity Legal Form",
      "entityLegalForm.disabled.placeholder": "Entity Legal Form",
      "entityLegalForm.select.placeholder": "--- Select ---",
      "entityLegalForm.optgroup": "Operated by NCSD",
      "entityLegalFormOther.optgroup": "Other",
      "otherLegalForm": "Other Legal Form",
      "otherLegalForm.placeholder": "Other Legal Form",
      "registrationCountry": "Legal Jurisdiction",
      "registrationCountry.placeholder": "Legal Jurisdiction",
      "registrationCountry.select.placeholder": "--- Select ---",
      "registrationCountry.any.placeholder": "Any country",
      "registrationRegion": "Legal Jurisdiction Region",
      "registrationRegion.placeholder": "Legal Jurisdiction Region",
      "registrationRegion.select.placeholder": "--- Select ---",
      "firstAddressLine": "Street",
      "firstAddressLine.placeholder": "Street",
      "addressNumber": "House",
      "addressNumber.placeholder": "House",
      "addressNumberWithinBuilding": "Suite",
      "addressNumberWithinBuilding.placeholder": "Suite",
      "city": "City",
      "city.placeholder": "City",
      "region": "Region",
      "region.placeholder": "Region",
      "region.select.placeholder": "--- Select ---",
      "country": "Country",
      "country.placeholder": "Country",
      "country.select.placeholder": "--- Select ---",
      "country.optgroup": "Operated by NCSD",
      "countryOther.optgroup": "Other",
      "postalCode": "Postal Code",
      "postalCode.placeholder": "Postal Code",
      "headquartersFirstAddressLine": "Street",
      "headquartersFirstAddressLine.placeholder": "Street",
      "headquartersAddressNumber": "House",
      "headquartersAddressNumber.placeholder": "House",
      "headquartersAddressNumberWithinBuilding": "Suite",
      "headquartersAddressNumberWithinBuilding.placeholder": "Suite",
      "headquartersCity": "City",
      "headquartersCity.placeholder": "City",
      "headquartersRegion": "Region",
      "headquartersRegion.placeholder": "Region",
      "headquartersRegion.disabled.placeholder": "Region",
      "headquartersRegion.select.placeholder": "--- Select ---",
      "headquartersCountry": "Country",
      "headquartersCountry.placeholder": "Country",
      "headquartersCountry.select.placeholder": "--- Select ---",
      "headquartersPostalCode": "Postal Code",
      "headquartersPostalCode.placeholder": "Postal Code",
      "registrationAuthorityID": "Registration Authority ID",
      "registrationAuthorityID.placeholder": "Registration Authority ID",
      "registrationAuthorityID.disabled.placeholder": "Registration Authority ID",
      "registrationAuthorityID.select.placeholder": "--- Select ---",
      "registrationAuthority.optgroup": "Operated by NCSD",
      "registrationAuthorityOther.optgroup": "Other",
      "registrationAuthorityEntityID": "Reg. Authority Entity ID",
      "registrationAuthorityEntityID.placeholder": "Reg. Authority Entity ID",
      "registrationAuthorityEntityID.select.placeholder": "--- Select ---",
      "contentDate": "Content Date",
      "fileContent": "File Content",
      "originator": "Originator",
      "recordCount": "Record Count",
      "addressType": "Address Type",
      "associatedLEI": "Associated LEI",
      "associatedEntityType": "Associated Entity Type",
      "entityCategoryType": "Entity Category",
      "entityCategoryType.placeholder": "Entity Category",
      "entityCategoryType.select.placeholder": "--- Select ---",
      "entityExpirationDate": "Entity Expiration Date",
      "entityExpirationReason": "Entity Expiration Reason",
      "entityExpirationReason.placeholder": "Entity Expiration Reason",
      "entityExpirationReason.select.placeholder": "--- Select ---",
      "entityNameType": "Entity Name Type",
      "entityStatus": "Entity Status",
      "entityStatus.placeholder": "Entity Status",
      "entityStatus.select.placeholder": "--- Select ---",
      "registrationStatus": "Registration Status",
      "managingLOU": "Managing LOU",
      "initialRegistrationDate": "Registration Date",
      "initialRegistrationDate.placeholder": "Initial Registration Date of Legal Entity in format YYYY-MM-DD",
      "lastUpdateDate": "Last Update Date",
      "lastUpdateDate.placeholder": "Last Update Date of Legal Entity in format YYYY-MM-DD",
      "nextRenewalDate": "Next Renewal Date",
      "submitDate": "Submit Date",
      "nextRenewalDate.placeholder": "Next Renewal Date is calculated as 1 Year from day, when LEI is requested",
      "isPaid": "Payment completed",
      "paymentDescription": "Press Payment to confirm or adjust payment record data.",
      "paymentDoneDescription": "Payment is done. Press Payment to adjust payment record data.",
      "paymentRecordNotFound": "Payment record not found. Please create payment manually or reject in order to make payment first before submit.",
      "paymentRecordOptionalNotFound": "Payment record not found, it is optional for transfer operation.",
      "isLeiDataValidated": "LEI data validated",
      "leiDataDescription": "Press Confirm to confirm all LEI data attributes below are filled correctly.",
      "leiDataDoneDescription": "LEI record is ready for validation. Or Press Adjust to make back editable of LEI record attributes.",
      "leiDataPublishDescription": "LEI record is valid and ready for publishing.",
      "isSubmitterValidated": "Submitter validated",
      "submitterDescription": "Press Confirm to confirm Submitter data listed below is correct.",
      "submitterDoneDescription": "Correctness of Submitter data listed below is confirmed.",
      "isDuplicateCheckDone": "Duplicates check done",
      "duplicateDescription": "Press Perform to check the LEI and LE-RD data for duplicate entries.",
      "duplicateDoneDescription": "Duplicate validation of the LEI and LE-RD data is done.",
      "duplicateTransferDescription": "Prior Duplicate Check please first accept or reject transfer operation.",
      "isDataQualityCheckDone": "Data quality check done",
      "dataQualityDescription": "Press Perform to check the LEI and LE-RD data against data quality rules.",
      "dataQualityDoneDescription": "Data Quality validation of the LEI and LE-RD data is done.",
      "dataQualityTransferDescription": "Prior Data Quality Check please first accept or reject transfer operation.",
      "userAuthority.primary": "Member of board",
      "userAuthority.assisted": "Power of attorney",
      "successorLEI": "Successor LEI",
      "successorEntityNameValue": "Successor Entity Name",
      "successorEntityNameLang": "Successor Entity Name Language",
      "validationAuthorityID": "Validation Authority ID",
      "otherValidationAuthorityID": "Other Validation Authority ID",
      "validationAuthorityEntityID": "Validation Authority Entity ID",
      "validationSources": "Validation Sources",
      "legalForm": "Legal Form",
      "idForVat": "VAT ID",
      "idForVat.placeholder": "VAT ID",
      "idForVatMeans": "Optional field for payment needs. If not provided, calculated price amount for services will include VAT.",
      "otherEntityNames": "Other Entity Names",
      "editle": "Edit Legal Entity",
      "resetle": "Restore original Legal Entity",
      "editla": "Edit Legal Address",
      "resetla": "Restore original Legal Address",
      "editha": "Edit Headquarters Address",
      "resetha": "Restore original Headquarters Address",
      "copy": "Copy from Legal Address",
      "copy.placeholder": "Copy from Legal Address",
      "related.actions": "Related actions"
    },
    "leiRelationshipData": {
      "messages": {
        "validate": {
          "dacLei": {
            "search": "Please search for Direct Parent LEI.",
            "required": "Direct Parent LEI is required."
          },
          "dacLegalName": {
            "required": "Parent Legal Name is required."
          },
          "dacRegistrationAuthorityEntityID": {
            "required": "Registration Number is required."
          },
          "dacRegistrationAuthorityID": {
            "required": "Registration Authority is required."
          },
          "dacFirstAddressLine": {
            "required": "First Address Line is required."
          },
          "dacCity": {
            "required": "City is required."
          },
          "dacCountry": {
            "required": "Country is required."
          },
          "dacRegistrationStatus": {
            "required": "Registration Status is required."
          },
          "dacValidationSources": {
            "required": "Validation Sources is required."
          },
          "dacQualifierCategory": {
            "required": "Accounting Consolidation Standard is required."
          },
          "uacLei": {
            "search": "Please search for Ultimate Parent LEI.",
            "required": "Ultimate Parent LEI is required."
          },
          "uacLegalName": {
            "required": "Parent Legal Name is required."
          },
          "uacRegistrationAuthorityEntityID": {
            "required": "Registration Number is required."
          },
          "uacRegistrationAuthorityID": {
            "required": "Registration Authority is required."
          },
          "uacFirstAddressLine": {
            "required": "First Address Line is required."
          },
          "uacCity": {
            "required": "City is required."
          },
          "uacCountry": {
            "required": "Country is required."
          },
          "uacRegistrationStatus": {
            "required": "Registration Status is required."
          },
          "uacValidationSources": {
            "required": "Validation Sources is required."
          },
          "uacQualifierCategory": {
            "required": "Accounting Consolidation Standard is required."
          },
          "parentLei": {
            "documents": "Please enclose document(s) stating that the Parent-Child relationship exists (consolidated financial statements, regulatory filings, Contracts, or other documents supporting the said relationship)\n                                                   "
          }
        }
      },
      "dacSearch": {
        "info": "The Direct Parent is the lowest level legal entity preparing consolidated financial statements for the child entity.",
        "info2": "Each Direct Parent LEI must be a valid, existing LEI code.",
        "info3": "If the entity does not currently have an LEI assigned, please register the Direct Parent entity prior to registering subsidiary entities."
      },
      "uacSearch": {
        "info": "The Ultimate Parent is the lowest level legal entity preparing consolidated financial statements for the child entity.",
        "info2": "Each Ultimate Parent LEI must be a valid, existing LEI code.",
        "info3": "If the entity does not currently have an LEI assigned, please register the Ultimate Parent entity prior to registering subsidiary entities."
      },
      "dacParent": "Direct Parent",
      "dacParent.placeholder": "Direct Parent",
      "dacParent.select.placeholder": "--- Select ---",
      "dacParent.search.placeholder": "Open search form for Direct Parent LEI.",
      "uacParent": "Utimate Parent",
      "uacParent.placeholder": "Utimate Parent",
      "uacParent.select.placeholder": "--- Select ---",
      "uacParent.search.placeholder": "Open search form for Ultimate Parent LEI.",
      "dacLei": "Direct Parent LEI",
      "dacLei.placeholder": "Direct Parent LEI",
      "dacPni": "Direct Parent PNI",
      "dacPni.placeholder": "Direct Parent PNI",
      "noDacLei": "Direct Parent has no LEI",
      "uacLei": "Utimate Parent LEI",
      "uacLei.placeholder": "Utimate Parent LEI",
      "uacPni": "Utimate Parent PNI",
      "uacPni.placeholder": "Utimate Parent PNI",
      "noUacLei": "Utimate Parent has no LEI",
      "dacLegalName": "Parent Legal Name",
      "dacLegalName.placeholder": "Parent Legal Name",
      "uacLegalName": "Parent Legal Name",
      "uacLegalName.placeholder": "Parent Legal Name",
      "dacLegalAddress": "Legal Address",
      "dacLegalAddress.placeholder": "Legal Address",
      "uacLegalAddress": "Legal Address",
      "uacLegalAddress.placeholder": "Legal Address",
      "dacFirstAddressLine": "Street",
      "dacFirstAddressLine.placeholder": "Street",
      "uacFirstAddressLine": "Street",
      "uacFirstAddressLine.placeholder": "Street",
      "dacAddressNumber": "Number",
      "dacAddressNumber.placeholder": "Number",
      "uacAddressNumber": "Number",
      "uacAddressNumber.placeholder": "Number",
      "dacAddressNumberWithinBuilding": "House",
      "dacAddressNumberWithinBuilding.placeholder": "House",
      "uacAddressNumberWithinBuilding": "Suite",
      "uacAddressNumberWithinBuilding.placeholder": "Suite",
      "dacAdditionalAddressLine": "Additional Address Line",
      "dacAdditionalAddressLine.placeholder": "Additional Address Line",
      "uacAdditionalAddressLine": "Additional Address Line",
      "uacAdditionalAddressLine.placeholder": "Additional Address Line",
      "dacCity": "City",
      "dacCity.placeholder": "City",
      "uacCity": "City",
      "uacCity.placeholder": "City",
      "dacRegion": "Region",
      "dacRegion.placeholder": "Region",
      "dacRegion.disabled.placeholder": "Region",
      "dacRegion.select.placeholder": "--- Select ---",
      "uacRegion": "Region",
      "uacRegion.placeholder": "Region",
      "uacRegion.disabled.placeholder": "Region",
      "uacRegion.select.placeholder": "--- Select ---",
      "dacCountry": "Country",
      "dacCountry.placeholder": "Country",
      "dacCountry.disabled.placeholder": "Country",
      "dacCountry.select.placeholder": "--- Select ---",
      "uacCountry": "Country",
      "uacCountry.placeholder": "Country",
      "uacCountry.disabled.placeholder": "Country",
      "uacCountry.select.placeholder": "--- Select ---",
      "dacPostalCode": "Postal Code",
      "dacPostalCode.placeholder": "Postal Code",
      "uacPostalCode": "Postal Code",
      "uacPostalCode.placeholder": "Postal Code",
      "dacRegistrationAuthorityID": "Registration Authority",
      "dacRegistrationAuthorityID.placeholder": "Registration Authority",
      "dacRegistrationAuthorityID.disabled.placeholder": "Registration Authority",
      "dacRegistrationAuthorityID.select.placeholder": "--- Select ---",
      "uacRegistrationAuthorityID": "Registration Authority",
      "uacRegistrationAuthorityID.placeholder": "Registration Authority",
      "uacRegistrationAuthorityID.disabled.placeholder": "Registration Authority",
      "uacRegistrationAuthorityID.select.placeholder": "--- Select ---",
      "dacRegistrationAuthorityEntityID": "Registration Number",
      "dacRegistrationAuthorityEntityID.placeholder": "Registration Number",
      "uacRegistrationAuthorityEntityID": "Registration Number",
      "uacRegistrationAuthorityEntityID.placeholder": "Registration Number",
      "dacRegistrationStatus": "Registration Status",
      "dacRegistrationStatus.placeholder": "Registration Status",
      "dacRegistrationStatus.disabled.placeholder": "Registration Status",
      "dacRegistrationStatus.select.placeholder": "--- Select ---",
      "uacRegistrationStatus": "Registration Status",
      "uacRegistrationStatus.placeholder": "Registration Status",
      "uacRegistrationStatus.disabled.placeholder": "Registration Status",
      "uacRegistrationStatus.select.placeholder": "--- Select ---",
      "dacValidationSources": "Validation Sources",
      "dacValidationSources.placeholder": "Validation Sources",
      "dacValidationSources.disabled.placeholder": "Validation Sources",
      "dacValidationSources.select.placeholder": "--- Select ---",
      "uacValidationSources": "Validation Sources",
      "uacValidationSources.placeholder": "Validation Sources",
      "uacValidationSources.disabled.placeholder": "Validation Sources",
      "uacValidationSources.select.placeholder": "--- Select ---",
      "dacQualifierCategory": "Accounting Consolidation Standard",
      "dacQualifierCategory.placeholder": "Accounting Consolidation Standard",
      "dacQualifierCategory.disabled.placeholder": "Accounting Consolidation Standard",
      "dacQualifierCategory.select.placeholder": "--- Select ---",
      "uacQualifierCategory": "Accounting Consolidation Standard",
      "uacQualifierCategory.placeholder": "Accounting Consolidation Standard",
      "uacQualifierCategory.disabled.placeholder": "Accounting Consolidation Standard",
      "uacQualifierCategory.select.placeholder": "--- Select ---",
      "dacRelationshipPeriod": "Relationship Period",
      "dacRelationshipPeriodStartDate": "Start Date",
      "dacRelationshipPeriodEndDate": "End Date",
      "uacRelationshipPeriod": "Relationship Period",
      "uacRelationshipPeriodStartDate": "Start Date",
      "uacRelationshipPeriodEndDate": "End Date",
      "isUacSameAsDac": "Same as Direct Parent",
      "copy": "Copy from Direct Parent",
      "copy.placeholder": "Copy from Direct Parent",
      "view.direct.label": "Previously submitted Direct Parent information",
      "edit.direct.label": "Renew Direct Parent information from prev.submitted data",
      "edit.direct.button": "Renew Direct Parent",
      "edit.direct.placeholder": "Renew Direct Parent information",
      "view.ultimate.label": "Previously submitted Ultimate Parent information",
      "edit.ultimate.label": "Renew Ultimate Parent information from prev.submitted data",
      "edit.ultimate.button": "Renew Ultimate Parent",
      "edit.ultimate.placeholder": "Renew Ultimate Parent information"
    },
    "leiReportingExceptionData": {
      "messages": {
        "validate": {
          "dacReason": {
            "required": "Reason is required."
          },
          "uacReason": {
            "required": "Reason is required."
          }
        }
      },
      "dacReason": "Reason",
      "dacReason.placeholder": "Reason",
      "dacReason.select.placeholder": "--- Select ---",
      "uacReason": "Reason",
      "uacReason.placeholder": "Reason",
      "uacReason.select.placeholder": "--- Select ---"
    },
    "transfer": {
      "tabs": {
        "details": "Transfer details",
        "details.in": "Transfer details: Transfer In",
        "details.out": "Transfer details: Transfer Out",
        "leiData": "LEI and LOU details",
        "authData": "Authorized representative",
        "comments": "Comments",
        "reason": "Reason"
      },
      "messages": {
        "validate": {
          "userAuthority": {
            "required": "User authority selection is required."
          },
          "attachment": {
            "required": "Upload authorization letter is required.",
            "direct": {
              "required": "Direct Parent document(s) are required."
            },
            "ultimate": {
              "required": "Ultimate Parent document(s) are required."
            }
          },
          "required.for.request": "No enough attributes passed validation in order to submit transfer request.",
          "required.for.objection": "No enough attributes passed validation in order to submit transfer objection.",
          "required.for.submit": "No enough attributes passed validation in order to perform transfer submission."
        }
      },
      "detail": {
        "tabs": {
          "legalEntity": "Legal Entity Information",
          "legalEntity.placeholder": "Legal Entity Information",
          "address": "Address",
          "address.placeholder": "Address",
          "relationship": "Relationship",
          "relationship.placeholder": "Relationship"
        },
        "title": "Transfer - LEI Details",
        "title.renew": "Transfer and Renew - LEI Details"
      },
      "request": {
        "title.in": "Transfer In - Request",
        "title.out": "Transfer Out - Request",
        "title.renew": "Transfer and Renew - Request"
      },
      "objection": {
        "title": "Transfer Objection"
      },
      "relationship": {
        "tabs": {
          "direct": "Relationship - Direct Parent",
          "direct.placeholder": "Relationship - Direct Parent",
          "ultimate": "Relationship - Ultimate Parent",
          "ultimate.placeholder": "Relationship - Ultimate Parent"
        },
        "title": "Transfer - Address / Relationship",
        "title.renew": "Transfer and Renew - Address / Relationship"
      },
      "submit": {
        "title": "Transfer - Submit",
        "title.renew": "Transfer and Renew - Submit"
      },
      "confirm": {
        "title": "Transfer - Confirmation",
        "title.renew": "Transfer and Renew - Confirmation"
      },
      "userAuthority": "User authority",
      "userAuthority.placeholder": "User authority",
      "userAuthority.primary": "Member of board",
      "userAuthority.assisted": "Power of attorney",
      "attachment": "Upload authorization letter.",
      "attachment.size": "Upload authorization letter. Maximum file size: 16 MB.",
      "attachment.valid.extension": "We can accept only pdf and jpg format.",
      "attachment.placeholder": "Authorization letter",
      "attachment.relationship": "List the titles of the document(s) stating that the Parent-Child relationship exists and enclose a copy of the said document(s) (consolidated financial statements, regulatory filings, Contracts, or other documents supporting the said relationship).<br/>Maximum size of all files: 16 MB.",
      "attachment.relationship.placeholder": "Direct and Ultimate Parent document(s)",
      "attachment.open.placeholder": "Open attachment file",
      "attachment.remove.placeholder": "Remove attachment file",
      "contact": "Please review transfer request and contact sending LOU, then you will be able to accept transfer request.",
      "pendingButton": "LEI record transfer pending",
      "acceptedButton": "LEI record transfer accepted",
      "rejectedButton": "LEI record transfer rejected",
      "accept.in": "Accept Transfer In",
      "accept.out": "Accept Archival",
      "complete.out": "Complete Archival",
      "reject.in": "Reject Transfer In",
      "reject.out": "Reject",
      "description.in.accept": "<b>Accepting Transfer In</b> request means putting transfer as done and allowing LEI record to publish after validation successful.",
      "description.in.reject": "<b>Rejecting Transfer In</b> request means cancelling transfer of LEI record to Nasdaq CSD and ready to it's archival.",
      "description.out.accept": "<b>Accepting Transfer Out</b> request means that this LEI record will not managed by Nasdaq CSD anymore, and it's ready to archival.",
      "description.out.reject": "<b>Rejecting Transfer Out</b> request means cancelling transfer of LEI record from Nasdaq CSD and restore it's previous registration status.",
      "description.pending.archival": "<b>Registration status</b> can be set manually to <b>PENDING_ARCHIVAL</b> in case of Transfer In and when Sending LOU confirmed it.",
      "accepted.in": "Transfer In request accepted, please proceed LEI record validation and publishing.",
      "accepted.out": "Transfer Out request accepted, LEI record is ready for archival (will not be managed by NCSD).",
      "rejected.in": "Transfer In request rejected, LEI record is ready for archival (will not be managed by NCSD).",
      "rejected.out": "Transfer Out request rejected, LEI record is restored it's previous registration status.",
      "view.request": "Request",
      "objections": "Objections",
      "objections.notfound": "Pending or blocking objections are not found.",
      "objections.found": "Pending or blocking objections are found ({{ count }}), please verify.",
      "submitted": "Please review request and objections, then approve or reject request."
    },
    "renew": {
      "messages": {
        "validate": {
          "userAuthority": {
            "required": "User authority selection is required."
          },
          "attachment": {
            "required": "Upload authorization letter is required.",
            "direct": {
              "required": "Direct Parent document(s) are required."
            },
            "ultimate": {
              "required": "Ultimate Parent document(s) are required."
            }
          },
          "required.for.submit": "No enough attributes passed validation in order to perform renewal."
        }
      },
      "detail": {
        "tabs": {
          "legalEntity": "Legal Entity Information",
          "legalEntity.placeholder": "Legal Entity Information",
          "address": "Address",
          "address.placeholder": "Address",
          "relationship": "Relationship",
          "relationship.placeholder": "Relationship"
        },
        "title": "Renewal - LEI Details"
      },
      "relationship": {
        "tabs": {
          "direct": "Relationship - Direct Parent",
          "direct.placeholder": "Relationship - Direct Parent",
          "ultimate": "Relationship - Ultimate Parent",
          "ultimate.placeholder": "Relationship - Ultimate Parent"
        },
        "title": "Renewal - Relationship"
      },
      "submit": {
        "title": "Renewal - Submit"
      },
      "confirm": {
        "title": "Renewal - Confirmation"
      },
      "userAuthority": "User authority",
      "userAuthority.placeholder": "User authority",
      "userAuthority.primary": "Primary Party renewal",
      "userAuthority.assisted": "Assisted renewal",
      "attachment": "Upload authorization letter",
      "attachment.size": "Upload authorization letter. Maximum file size: 16 MB.",
      "attachment.placeholder": "Authorization letter",
      "attachment.relationship": "List the titles of the document(s) stating that the Parent-Child relationship exists and enclose a copy of the said document(s) (consolidated financial statements, regulatory filings, Contracts, or other documents supporting the said relationship).<br/>Maximum size of all files: 16 MB.",
      "attachment.relationship.placeholder": "Direct and Ultimate Parent document(s)",
      "attachment.open.placeholder": "Open attachment file",
      "attachment.remove.placeholder": "Remove attachment file"
    },
    "challenge": {
      "messages": {
        "validate": {
          "challengeInfo": {
            "required": "Challenge data information is required."
          },
          "challengeAuth": {
            "required": "Authoritative source of information is required."
          },
          "required.for.request": "No enough attributes passed validation in order to submit challenge request."
        }
      },
      "detail": {
        "tabs": {
          "legalEntity": "Legal Entity Information",
          "legalEntity.placeholder": "Legal Entity Information",
          "address": "Address",
          "address.placeholder": "Address",
          "relationship": "Relationship",
          "relationship.placeholder": "Relationship",
          "challengeRequest": "Challenge Request",
          "challengeRequest.placeholder": "Challenge Request",
          "attachments": "Enclosed Documents: {{count}}",
          "attachments.placeholder": "Enclosed Documents - optional data"
        }
      },
      "request": {
        "title": "Challenge - Request"
      },
      "submit": {
        "title": "Challenge data"
      },
      "confirm": {
        "title": "Challenge - Confirmation"
      },
      "challengeInfo": "Challenge data information",
      "challengeInfo.placeholder": "Enter brief description of changes",
      "challengeAuth": "Authoritative source of information",
      "challengeAuth.placeholder": "Fill authoritative source of information",
      "attachment": "Upload authorization letter",
      "attachment.size": "Upload authorization letter. Maximum file size: 16 MB.",
      "attachment.placeholder": "Authorization letter",
      "attachment.relationship": "List the titles of the document(s) stating that the Parent-Child relationship exists and enclose a copy of the said document(s) (consolidated financial statements, regulatory filings, Contracts, or other documents supporting the said relationship).<br/>Maximum size of all files: 16 MB.",
      "attachment.relationship.placeholder": "Direct and Ultimate Parent document(s)",
      "attachment.open.placeholder": "Open attachment file",
      "attachment.remove.placeholder": "Remove attachment file"
    }
  }
}
